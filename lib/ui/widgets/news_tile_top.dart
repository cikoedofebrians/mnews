import 'package:flutter/material.dart';
import 'package:mnews/utils/constant.dart';

class NewsTileTop extends StatelessWidget {
  const NewsTileTop({
    super.key,
    required this.name,
    required this.url,
  });

  final String name;
  final String url;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 15),
      height: 200,
      width: 150,
      child: Stack(children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: NetworkImage(url), fit: BoxFit.cover),
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment.center,
                colors: [Colors.black.withOpacity(0.8), Colors.transparent],
              ),
              borderRadius: BorderRadius.circular(20)),
        ),
        Positioned(
          bottom: 10,
          left: 10,
          right: 0,
          child: Text(
            name,
            style: Theme.of(context)
                .textTheme
                .titleSmall!
                .copyWith(color: Colors.white),
          ),
        ),
      ]),
    );
  }
}
