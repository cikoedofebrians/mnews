import 'package:flutter/material.dart';
import 'package:mnews/ui/widgets/news_tile_top.dart';
import 'package:mnews/utils/constant.dart';

class NewsTopBuilder extends StatelessWidget {
  const NewsTopBuilder({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      height: 200,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemBuilder: (context, index) => NewsTileTop(
          name: trendingTopic[index]['name']!,
          url: trendingTopic[index]['url']!,
        ),
        itemCount: trendingTopic.length,
      ),
    );
  }
}
