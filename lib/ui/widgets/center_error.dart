import 'package:flutter/material.dart';

class CenterError extends StatelessWidget {
  const CenterError({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.signal_wifi_connected_no_internet_4_rounded,
          ),
          Text(
            'Something went wrong, please try it again!',
          ),
        ],
      ),
    );
  }
}
