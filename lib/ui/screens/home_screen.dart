import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mnews/logic/cubit/cubit/news_cubit_cubit.dart';
import 'package:mnews/ui/widgets/center_error.dart';
import 'package:mnews/ui/widgets/news_tile_bottom.dart';
import 'package:mnews/ui/widgets/news_top_builder.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: BlocBuilder<NewsCubit, NewsCubitState>(
      builder: (context, state) {
        if (state is NewsCubitLoading) {
          return const Center(child: CircularProgressIndicator());
        }

        if (state is NewsCubitFailed) {
          return const CenterError();
        }

        final data = state as NewsCubitSuccess;
        return SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Trending',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      Text(
                        'Follow the current trend',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const NewsTopBuilder(),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                  child: Text(
                    'Popular News',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ),
                ...List.generate(
                  10,
                  (index) => NewsTileBottom(article: data.list[index]),
                ),
              ],
            ),
          ),
        );
      },
    ));
  }
}
