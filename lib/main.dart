import 'package:flutter/material.dart';
import 'package:mnews/logic/cubit/cubit/news_cubit_cubit.dart';
import 'package:mnews/ui/screens/home_screen.dart';
import 'package:mnews/ui/screens/news_details.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MKnews',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: const TextTheme(
            titleLarge: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            titleMedium: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
            titleSmall: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            bodyMedium: TextStyle(fontWeight: FontWeight.normal, fontSize: 14)),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      routes: {
        '/': (context) => BlocProvider(
              create: (context) => NewsCubit()..getData(),
              child: const HomeScreen(),
            ),
        '/news-details': (context) => const NewsDetails()
      },
    );
  }
}
