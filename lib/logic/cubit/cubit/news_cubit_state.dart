part of 'news_cubit_cubit.dart';

sealed class NewsCubitState extends Equatable {}

final class NewsCubitInitial extends NewsCubitState {
  final int row = 0;

  @override
  List<Object?> get props => [row];
}

final class NewsCubitLoading extends NewsCubitState {
  @override
  List<Object?> get props => [];
}

final class NewsCubitSuccess extends NewsCubitState {
  final List<Article> list;

  NewsCubitSuccess({
    required this.list,
  });

  @override
  List<Object?> get props => [list];
}

final class NewsCubitFailed extends NewsCubitState {
  @override
  List<Object?> get props => [];
}
