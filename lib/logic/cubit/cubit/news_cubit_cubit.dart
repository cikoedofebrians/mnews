import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:mnews/data/model/article.dart';
import 'package:mnews/utils/constant.dart';
part 'news_cubit_state.dart';

class NewsCubit extends Cubit<NewsCubitState> {
  NewsCubit() : super(NewsCubitInitial());

  void getData() async {
    final dio = Dio();

    try {
      emit(NewsCubitLoading());
      final rawData = await dio.get(popularNews);
      final list = rawData.data['articles'] as List<dynamic>;
      final List<Article> convertedList =
          list.map((e) => Article.fromJson(e)).toList();
      emit(NewsCubitSuccess(list: convertedList));
    } catch (err) {
      emit(NewsCubitFailed());
    }
  }
}
