const noPhotoAvailable =
    "https://archive.org/download/no-photo-available/no-photo-available.png";
const apiKey = "f20e30e90d10493aa9f378fbd3aa8fdc";

const popularNews =
    "https://newsapi.org/v2/everything?sortBy=popularity&apiKey=$apiKey";

const trendingTopic = [
  {
    'name': "Apple",
    "url":
        'https://www.apple.com/ac/structured-data/images/open_graph_logo.png?202209082218',
  },
  {
    'name': 'Tesla',
    'url':
        'https://static.republika.co.id/uploads/images/inpicture_slide/a-tesla-logo-on-a-model-s-is-photographed_230512104105-602.jpeg'
  },
  {
    'name': 'Stock',
    'url':
        'https://g.foolcdn.com/editorial/images/721776/growth-stock-chart.jpg'
  },
  {
    'name': 'PlayStation',
    'url':
        'https://yt3.googleusercontent.com/ytc/AOPolaTDMLFMku3rto1KBMFVx9KrTbgQvPF3ak20u9eDmYM=s900-c-k-c0x00ffffff-no-rj'
  },
  {
    'name': 'Programming',
    'url':
        'https://www.codingem.com/wp-content/uploads/2021/10/juanjo-jaramillo-mZnx9429i94-unsplash-1024x683.jpg'
  }
];
